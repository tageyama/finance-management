import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store'
import dayjs from 'dayjs'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    beforeEnter: (to, from, next) => {
      next({ name: 'Attendance' })
    }
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/UserManage.vue')
  },
  {
    path: '/attendance',
    name: 'Attendance',
    component: () => import('../views/Attendance.vue'),
    props: route => {
      const dates = store.getters['setting/dates'].attendance
      return {
        year: route.query.year || dates.year || String(dayjs().year()),
        month: route.query.month || dates.month || String(dayjs().month()  +1),
        day: route.query.day || dates.day || '0',
        weekday: dates.weekday || route.query.weekday
      }
    }
  },
  {
    path: '/incomespending',
    name: 'IncomeSpending',
    component: () => import('../views/IncomeSpending.vue'),
    props: route => {
      const dates = store.getters['setting/dates'].incomeSpending
      return {
        year: route.query.year || dates.year || String(dayjs().year()),
        month: route.query.month || dates.month || String(dayjs().month() + 1),
        day: route.query.day || dates.day || '0',
        weekday: dates.weekday || route.query.weekday
      }
    }
  },
  {
    path: '/paymentsbalance',
    name: 'PaymentsBalance',
    component: () => import('../views/PaymentsBalance.vue'),
    props: route => {
      const dates = store.getters['setting/dates'].paymentsBalance
      return {
        year: route.query.year || dates.year || String(dayjs().year()),
        month: route.query.month || dates.month || String(dayjs().month() + 1),
        day: route.query.day || dates.day || '0',
        weekday: dates.weekday || route.query.weekday
      }
    }
  },
  {
    path: '/setting',
    name: 'Setting',
    component: () => import('../views/Setting.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
