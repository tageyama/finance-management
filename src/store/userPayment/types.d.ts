import UserPayment from '@/types/userPayment'
import { DateMonthString, DateSpan, Weekday } from '@/types'

export type UserPayments = {
  [id: number]: UserPayment
}

type UserPaymentPayload = {
  id: number
  payload: Partial<UserPayment>
}

type FilterQuery = {
  month?: DateMonthString
  span?: DateSpan
  userId?: number
}

/* -------------------------------------------------------------------------- */
export interface S {
  userPayments: UserPayments
}

/* -------------------------------------------------------------------------- */

export interface G {
  get: (id: number) => UserPayment | null
  all: UserPayment[]
  latestId: number
  getByMonth: (
    userId: number,
    month: DateMonthString
  ) => UserPayment | undefined
  filterByMonth: (
    month: DateMonthString,
    items?: UserPayment[]
  ) => UserPayment[]
  filterBySpan: (span: DateSpan, items?: UserPayment[]) => UserPayment[]
  filterByUser: (userId: number, items?: UserPayment[]) => UserPayment[]
  filterByQuery: (query: FilterQuery, items?: UserPayment[]) => UserPayment[]
}

export interface RG {
  'userPayment/get': G['get']
  'userPayment/all': G['all']
  'userPayment/latestId': G['latestId']
  'userPayment/filterByMonth': G['filterByMonth']
  'userPayment/filterBySpan': G['filterBySpan']
  'userPayment/filterByUser': G['filterByUser']
  'userPayment/filterByQuery': G['filterByQuery']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  addUserPayment: UserPayment
  updateUserPayment: UserPaymentPayload
  deleteUserPayment: { id: number }
}

export interface RM {
  'userPayment/replace': G['replace']
  'userPayment/addUserPayment': G['addUserPayment']
  'userPayment/updateUserPayment': G['updateUserPayment']
  'userPayment/deleteUserPayment': G['deleteUserPayment']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  addUserPayment: Omit<UserPayment, 'id'>
  updateUserPayment: UserPaymentPayload
  deleteUserPayment: { id: number }
}

export interface RA {
  'userPayment/replace': A['replace']
  'userPayment/addUserPayment': A['addUserPayment']
  'userPayment/updateUserPayment': A['updateUserPayment']
  'userPayment/deleteUserPayment': A['deleteUserPayment']
}

/* -------------------------------------------------------------------------- */
