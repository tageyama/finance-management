import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import dayjs from 'dayjs'
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  userPayments: {}
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  get: ({ userPayments }) => id => userPayments[id],
  all: ({ userPayments }) => Object.entries(userPayments).map(a => a[1]),
  latestId: ({ userPayments }) => {
    const ids = Object.keys(userPayments).map(i => Number(i))
    if (!ids.length) return -1
    return Math.max(...ids)
  },
  getByMonth: ({ userPayments }, getters) => (userId, month) => {
    const m = getters['filterByMonth'](month)
    return m.find(up => up.user == userId)
  },
  filterByMonth: ({ userPayments }, getters) => (month, items) => {
    const target = items || getters['all']
    return target.filter(a => a.date === month)
  },
  filterBySpan: ({ userPayments }, getters) => (span, items) => {
    const target = items || getters['all']
    return target.filter(a => {
      const from = dayjs(span.from)
      const to = dayjs(span.to)
      const target = dayjs(a.date)
      return (
        (from.isSame(target) || from.isAfter(target)) && to.isBefore(target)
      )
    })
  },
  filterByUser: ({ userPayments }, getters) => (userId, items) => {
    const target = items || getters['all']
    return target.filter(a => a.user === userId)
  },
  filterByQuery: ({ userPayments }, getters) => (query, items) => {
    let target = items || getters['all']
    const { userId, month, span } = query
    if (userId) target = getters['filterByUser'](userId, target)
    if (span) target = getters['filterBySpan'](span, target)
    if (month) target = getters['filterByMonth'](month, target)
    return target
  }
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.userPayments = newState.userPayments
  },
  addUserPayment: (state, userPayment) => {
    Vue.set(state.userPayments, userPayment.id, userPayment)
  },
  updateUserPayment: (state, { id, payload }) => {
    const userPayment = Object.assign({}, state.userPayments[id], payload)
    Vue.set(state.userPayments, id, userPayment)
  },
  deleteUserPayment: (state, { id }) => {
    Vue.delete(state.userPayments, id)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  addUserPayment: ({ state, commit, getters, rootState }, payload) => {
    const id = getters['latestId'] + 1
    const exist = getters['getByMonth'](payload.user, payload.date)
    if (exist) {
      commit('updateUserPayment', { id: exist.id, payload })
      return
    }
    commit('addUserPayment', Object.assign({}, payload, { id }))
    ipcHandler.saveState(rootState)
  },
  updateUserPayment: ({ commit, rootState }, payload) => {
    commit('updateUserPayment', payload)
    ipcHandler.saveState(rootState)
  },
  deleteUserPayment: ({ commit, rootState }, payload) => {
    commit('deleteUserPayment', payload)
    ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
