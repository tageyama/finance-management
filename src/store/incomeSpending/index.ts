import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import dayjs from 'dayjs'
import { weekday } from '@/types'
import { sortByDate } from '@/util'
import { incomeSpendings } from '@/dummy/incomeSpending' //TODO: delete
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  incomeSpendings: {}
  // incomeSpendings: incomeSpendings //TODO: delete
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  get: ({ incomeSpendings }) => id => incomeSpendings[id],
  all: ({ incomeSpendings }) => Object.entries(incomeSpendings).map(a => a[1]),
  latestId: ({ incomeSpendings }) => {
    const ids = Object.keys(incomeSpendings).map(i => Number(i))
    if (!ids.length) return -1
    return Math.max(...ids)
  },
  filterByDate: ({ incomeSpendings }, getters) => (date, items) => {
    const target = items || getters['all']
    return target.filter(a => a.date === date)
  },
  filterByDates: ({ incomeSpendings }, getters) => (dates, items) => {
    const target = items || getters['all']
    return target.filter(a => dates.includes(a.date))
  },
  filterByWeekday: ({ incomeSpendings }, getters) => (wday, items) => {
    const target = items || getters['all']
    return target.filter(a => weekday[dayjs(a.date).day()] === wday)
  },
  filterBySpan: ({ incomeSpendings }, getters) => (span, items) => {
    const target = items || getters['all']
    return target.filter(a => {
      const from = dayjs(span.from)
      const to = dayjs(span.to)
      const target = dayjs(a.date)
      return (
        from.isSame(target) || (target.isAfter(from) && target.isBefore(to))
      )
    })
  },
  filterByQuery: ({ incomeSpendings }, getters) => (query, items) => {
    let target = items || getters['all']
    const { date, dates, span, weekday, desc } = query
    if (span) target = getters['filterBySpan'](span, target)
    if (dates) target = getters['filterByDates'](dates, target)
    if (date) target = getters['filterByDate'](date, target)
    if (weekday) target = getters['filterByWeekday'](weekday, target)
    return getters['sortByDate'](!!desc, target)
  },
  sortByDate: ({ incomeSpendings }, getters) => (desc, items) => {
    const target = items || getters['all']
    const sorted = [...target]
    sortByDate(sorted)
    if (desc) sorted.reverse()
    return sorted
  }
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.incomeSpendings = newState.incomeSpendings
  },
  addIncomeSpending: (state, incomeSpending) => {
    Vue.set(state.incomeSpendings, incomeSpending.id, incomeSpending)
  },
  updateIncomeSpending: (state, { id, payload }) => {
    const incomeSpending = Object.assign({}, state.incomeSpendings[id], payload)
    Vue.set(state.incomeSpendings, id, incomeSpending)
  },
  deleteIncomeSpending: (state, { id }) => {
    Vue.delete(state.incomeSpendings, id)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  addIncomeSpending: ({ state, commit, getters, rootState }, payload) => {
    const id = getters['latestId'] + 1
    commit('addIncomeSpending', Object.assign({}, payload, { id }))
    ipcHandler.saveState(rootState)
  },
  updateIncomeSpending: ({ commit, rootState }, payload) => {
    commit('updateIncomeSpending', payload)
    ipcHandler.saveState(rootState)
  },
  deleteIncomeSpending: ({ commit, rootState }, payload) => {
    commit('deleteIncomeSpending', payload)
    ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
