import IncomeSpending from '@/types/incomeSpending'
import { DateString, DateSpan, Weekday } from '@/types'

export type IncomeSpendings = {
  [id: number]: IncomeSpending
}

type IncomeSpendingPayload = {
  id: number
  payload: Partial<IncomeSpending>
}

type FilterQuery = {
  date?: DateString
  dates?: DateString[]
  weekday?: Weekday
  span?: DateSpan
  desc?: boolean
}

/* -------------------------------------------------------------------------- */
export interface S {
  incomeSpendings: IncomeSpendings
}

/* -------------------------------------------------------------------------- */

export interface G {
  get: (id: number) => IncomeSpending | null
  all: IncomeSpending[]
  latestId: number
  filterByDate: (date: DateString, items?: IncomeSpending[]) => IncomeSpending[]
  filterByDates: (
    dates: DateString[],
    items?: IncomeSpending[]
  ) => IncomeSpending[]
  filterByWeekday: (wday: Weekday, items?: IncomeSpending[]) => IncomeSpending[]
  filterBySpan: (span: DateSpan, items?: IncomeSpending[]) => IncomeSpending[]
  filterByQuery: (
    query: FilterQuery,
    items?: IncomeSpending[]
  ) => IncomeSpending[]
  sortByDate: (desc = true, items?: IncomeSpending[]) => IncomeSpending[]
}

export interface RG {
  'incomeSpending/get': G['get']
  'incomeSpending/latestId': G['latestId']
  'incomeSpending/filterByDate': G['filterByDate']
  'incomeSpending/filterByDates': G['filterByDates']
  'incomeSpending/filterByWeekday': G['filterByWeekday']
  'incomeSpending/filterBySpan': G['filterBySpan']
  'incomeSpending/filterByQuery': G['filterByQuery']
  'incomeSpending/sortByDate': G['sortByDate']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  addIncomeSpending: IncomeSpending
  updateIncomeSpending: IncomeSpendingPayload
  deleteIncomeSpending: { id: number }
}

export interface RM {
  'incomeSpending/replace': G['replace']
  'incomeSpending/addIncomeSpending': G['addIncomeSpending']
  'incomeSpending/updateIncomeSpending': G['updateIncomeSpending']
  'incomeSpending/deleteIncomeSpending': G['deleteIncomeSpending']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  addIncomeSpending: Omit<IncomeSpending, 'id'>
  updateIncomeSpending: IncomeSpendingPayload
  deleteIncomeSpending: { id: number }
}

export interface RA {
  'incomeSpending/replace': A['replace']
  'incomeSpending/addIncomeSpending': A['addIncomeSpending']
  'incomeSpending/updateIncomeSpending': A['updateIncomeSpending']
  'incomeSpending/deleteIncomeSpending': A['deleteIncomeSpending']
}

/* -------------------------------------------------------------------------- */
