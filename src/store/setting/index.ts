import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  colors: {
    attendance: {
      menu: 'green',
      otherBalance: 'green',
      income: 'blue',
      balance: 'yellow'
    },
    incomeSpending: {
      menu: 'blue',
      memo: 'white',
      amount: 'white'
    },
    paymentsBalance: {
      menu: 'red',
      otherBalance: 'green',
      spending: 'red',
      income: 'blue',
      balance: 'yellow'
    },
    userManage: {
      menu: 'orange'
    },
    setting: {
      menu: 'darkgray'
    }
  },
  size: {
    attendance: 'm',
    incomeSpending: 'm',
    paymentsBalance: 'm',
    userManage: 'm'
  },
  dates: {
    attendance: { year: null, month: null, day: null, weekday: null },
    incomeSpending: { year: null, month: null, day: null, weekday: null },
    paymentsBalance: { year: null, month: null, day: null, weekday: null }
  }
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  colors: ({ colors }) => colors,
  size: ({ size }) => size,
  dates: ({ dates }) => dates
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.colors = newState.colors
    state.size = newState.size
    // state.dates = newState.dates
  },
  updateAttendanceColor: (state, payload) => {
    const c = state.colors.attendance
    Vue.set(state.colors, 'attendance', Object.assign({}, c, payload))
  },
  updateISColor: (state, payload) => {
    const c = state.colors.incomeSpending
    Vue.set(state.colors, 'incomeSpending', Object.assign({}, c, payload))
  },
  updatePBColor: (state, payload) => {
    const c = state.colors.paymentsBalance
    Vue.set(state.colors, 'paymentsBalance', Object.assign({}, c, payload))
  },
  updateSettingColor: (state, payload) => {
    const c = state.colors.setting
    Vue.set(state.colors, 'setting', Object.assign({}, c, payload))
  },
  updateUMColor: (state, payload) => {
    const c = state.colors.userManage
    Vue.set(state.colors, 'userManage', Object.assign({}, c, payload))
  },
  updateSize: (state, payload) => {
    const s = state.size
    state.size = Object.assign({}, s, payload)
  },
  updateDates: (state, payload) => {
    const d = state.dates
    state.dates = Object.assign({}, d, payload)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  updateAttendanceColor: ({ commit, rootState }, payload) => {
    commit('updateAttendanceColor', payload)
    ipcHandler.saveState(rootState)
  },
  updateISColor: ({ commit, rootState }, payload) => {
    commit('updateISColor', payload)
    ipcHandler.saveState(rootState)
  },
  updatePBColor: ({ commit, rootState }, payload) => {
    commit('updatePBColor', payload)
    ipcHandler.saveState(rootState)
  },
  updateSettingColor: ({ commit, rootState }, payload) => {
    commit('updateSettingColor', payload)
    ipcHandler.saveState(rootState)
  },
  updateUMColor: ({ commit, rootState }, payload) => {
    commit('updateUMColor', payload)
    ipcHandler.saveState(rootState)
  },
  updateSize: ({ commit, rootState }, payload) => {
    commit('updateSize', payload)
    ipcHandler.saveState(rootState)
  },
  updateDates: ({ commit, rootState }, payload) => {
    commit('updateDates', payload)
    // ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
