import { Color } from '@/types/color'
import { DisplaySize } from '@/components/organisms/FlexTable.vue'

type Date = {
  year: string | null
  month: string | null
  day: string | null
  weekday: string | null
}
type Dates = {
  attendance: Date
  incomeSpending: Date
  paymentsBalance: Date
}
type Size = {
  attendance: DisplaySize
  incomeSpending: DisplaySize
  paymentsBalance: DisplaySize
  userManage: DisplaySize
}
export type AttendanceColor = {
  menu: Color
  otherBalance: Color
  income: Color
  balance: Color
}
export type IncomeSpendingColor = {
  menu: Color
  memo: Color
  amount: Color
}
export type PaymentsBalanceColor = {
  menu: Color
  otherBalance: Color
  spending: Color
  income: Color
  balance: Color
}
export type UserManageColor = {
  menu: Color
}
export type SettingColor = {
  menu: Color
}
export type AppColor = {
  attendance: AttendanceColor
  incomeSpending: IncomeSpendingColor
  paymentsBalance: PaymentsBalanceColor
  userManage: UserManageColor
  setting: SettingColor
}

/* -------------------------------------------------------------------------- */
export interface S {
  colors: AppColor
  size: Size
  dates: Dates
}

/* -------------------------------------------------------------------------- */

export interface G {
  colors: AppColor
  size: Size
  dates: Dates
}

export interface RG {
  'setting/colors': G['colors']
  'setting/size': G['size']
  'setting/dates': G['dates']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  updateAttendanceColor: Partial<AttendanceColor>
  updateISColor: Partial<IncomeSpendingColor>
  updatePBColor: Partial<PaymentsBalanceColor>
  updateUMColor: Partial<UserManageColor>
  updateSettingColor: Partial<SettingColor>
  updateSize: Partial<Size>
  updateDates: Partial<Dates>
}

export interface RM {
  'setting/replace': G['replace']
  'setting/updateAttendanceColor': G['updateAttendanceColor']
  'setting/updateISColor': G['updateISColor']
  'setting/updatePBColor': G['updatePBColor']
  'setting/updateUMColor': G['updateUMColor']
  'setting/updateSettingColor': G['updateSettingColor']
  'setting/updateSize': G['updateSize']
  'setting/updateDates': G['updateDates']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  updateAttendanceColor: Partial<AttendanceColor>
  updateISColor: Partial<IncomeSpendingColor>
  updatePBColor: Partial<PaymentsBalanceColor>
  updateUMColor: Partial<UserManageColor>
  updateSettingColor: Partial<SettingColor>
  updateSize: Partial<Size>
  updateDates: Partial<Dates>
}

export interface RA {
  'setting/replace': A['replace']
  'setting/updateAttendanceColor': A['updateAttendanceColor']
  'setting/updateISColor': A['updateISColor']
  'setting/updatePBColor': A['updatePBColor']
  'setting/updateUMColor': A['updateUMColor']
  'setting/updateSettingColor': A['updateSettingColor']
  'setting/updateSize': A['updateSize']
  'setting/updateDates': A['updateDates']
}

/* -------------------------------------------------------------------------- */
