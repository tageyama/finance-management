import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import { users } from '@/dummy/user' //TODO: delete
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  users: {}
  // users: users // TODO: delete
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  get: ({ users }) => id => users[id],
  all: ({ users }) =>
    Object.entries(users)
      .map(u => u[1])
      .sort((a, b) => a.order - b.order),
  actives: (state, getters) => {
    const users = getters['all']
    return users.filter(u => !u.inactive)
  },
  latestId: ({ users }) => {
    if (users == {}) return -1
    const ids = Object.keys(users).map(i => Number(i))
    if (!ids.length) return -1
    return Math.max(...ids)
  },
  filterByName: ({ users }, getters) => (name, items) => {
    const target = items || getters['all']
    return target.filter(u => u.name.indexOf(name) > -1)
  },
  filterByPaymentType: ({ users }, getters) => (type, items) => {
    const target = items || getters['all']
    return target.filter(u => u.paymentType === type)
  },
  filterByWeekday: ({ users }, getters) => (wday, items) => {
    const target = items || getters['all']
    return target.filter(u => u.weekday.includes(wday))
  },
  filterByQuery: ({ users }, getters) => query => {
    const { name, includeInactive, weekday } = query
    let target = includeInactive ? getters['all'] : getters['actives']
    if (name) target = getters['filterByName'](name)
    if (weekday) target = getters['filterByWeekday'](weekday)
    return target
  }
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.users = newState.users
  },
  addUser: (state, user) => {
    Vue.set(state.users, user.id, user)
  },
  updateUser: (state, { id, payload }) => {
    const user = Object.assign({}, state.users[id], payload)
    Vue.set(state.users, id, user)
  },
  deleteUser: (state, { id }) => {
    Vue.delete(state.users, id)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  addUser: ({ state, commit, getters, rootState }, payload) => {
    const id = getters['latestId'] + 1
    commit('addUser', Object.assign({}, payload, { id, order: id }))
    ipcHandler.saveState(rootState)
    return id
  },
  updateUser: ({ commit, rootState }, payload) => {
    commit('updateUser', payload)
    ipcHandler.saveState(rootState)
  },
  deleteUser: ({ commit, rootState }, payload) => {
    commit('deleteUser', payload)
    ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
