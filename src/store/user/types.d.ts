import User from '@/types/user'
import { Weekday, PaymentType } from '@/types'

export type Users = {
  [id: number]: User
}

export type UserPayload = {
  id: number
  payload: Partial<User>
}

type Query = {
  name?: string
  includeInactive?: boolean
  weekday?: Weekday
}

/* -------------------------------------------------------------------------- */
export interface S {
  users: Users
}

/* -------------------------------------------------------------------------- */

export interface G {
  get: (id: number) => User | null
  all: User[]
  actives: User[]
  latestId: number
  filterByName: (name: string, items?: User[]) => User[]
  filterByPaymentType: (type: PaymentType, items?: User[]) => User[]
  filterByWeekday: (weekday: Weekday, items?: User[]) => User[]
  filterByQuery: (query: Query) => User[]
}

export interface RG {
  'user/get': G['get']
  'user/all': G['all']
  'user/actives': G['actives']
  'user/latestId': G['latestId']
  'user/filterByName': G['filterByName']
  'user/filterByWeekday': G['filterByWeekday']
  'user/filterByQuery': G['filterByQuery']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  addUser: User
  updateUser: UserPayload
  deleteUser: { id: number }
}

export interface RM {
  'user/replace': G['replace']
  'user/addUser': G['addUser']
  'user/updateUser': G['updateUser']
  'user/deleteUser': G['deleteUser']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  addUser: Omit<User, 'id' | 'order'>
  updateUser: UserPayload
  deleteUser: { id: number }
}

export interface RA {
  'user/replace': A['replace']
  'user/addUser': A['addUser']
  'user/updateUser': A['updateUser']
  'user/deleteUser': A['deleteUser']
}

/* -------------------------------------------------------------------------- */
