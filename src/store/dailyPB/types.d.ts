import DailyPB from '@/types/dailyPaymentsBalance'
import { DateString, DateSpan, Weekday } from '@/types'

export type DailyPBs = {
  [id: number]: DailyPB
}

type DailyPBPayload = {
  id: number
  payload: Partial<DailyPB>
}

type FilterQuery = {
  date?: DateString
  dates?: DateString[]
  weekday?: Weekday
  span?: DateSpan
  desc?: boolean
}

/* -------------------------------------------------------------------------- */
export interface S {
  dailyPBs: DailyPBs
}

/* -------------------------------------------------------------------------- */

export interface G {
  get: (id: number) => DailyPB | null
  all: DailyPB[]
  latestId: number
  filterByDate: (date: DateString, items?: DailyPB[]) => DailyPB | undefined
  filterByDates: (dates: DateString[], items?: DailyPB[]) => DailyPB[]
  filterByWeekday: (wday: Weekday, items?: DailyPB[]) => DailyPB[]
  filterBySpan: (span: DateSpan, items?: DailyPB[]) => DailyPB[]
  filterByQuery: (query: FilterQuery, items?: DailyPB[]) => DailyPB[]
  sortByDate: (desc = true, items?: DailyPB[]) => DailyPB[]
}

export interface RG {
  'dailyPB/get': G['get']
  'dailyPB/latestId': G['latestId']
  'dailyPB/filterByDate': G['filterByDate']
  'dailyPB/filterByDates': G['filterByDates']
  'dailyPB/filterByWeekday': G['filterByWeekday']
  'dailyPB/filterBySpan': G['filterBySpan']
  'dailyPB/filterByQuery': G['filterByQuery']
  'dailyPB/sortByDate': G['sortByDate']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  addDailyPB: DailyPB
  updateDailyPB: DailyPBPayload
  deleteDailyPB: { id: number }
}

export interface RM {
  'dailyPB/replace': G['replace']
  'dailyPB/addDailyPB': G['addDailyPB']
  'dailyPB/updateDailyPB': G['updateDailyPB']
  'dailyPB/deleteDailyPB': G['deleteDailyPB']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  addDailyPB: Omit<DailyPB, 'id'>
  updateDailyPB: DailyPBPayload
  deleteDailyPB: { id: number }
}

export interface RA {
  'dailyPB/replace': A['replace']
  'dailyPB/addDailyPB': A['addDailyPB']
  'dailyPB/updateDailyPB': A['updateDailyPB']
  'dailyPB/deleteDailyPB': A['deleteDailyPB']
}

/* -------------------------------------------------------------------------- */
