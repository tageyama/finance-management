import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import dayjs from 'dayjs'
import { weekday } from '@/types'
import { dailyPbs } from '@/dummy/dailyPb' // TODO: delete
import { sortByDate } from '@/util'
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  dailyPBs: {}
  // dailyPBs: dailyPbs // TODO: delete
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  get: ({ dailyPBs }) => id => dailyPBs[id],
  all: ({ dailyPBs }) => Object.entries(dailyPBs).map(a => a[1]),
  latestId: ({ dailyPBs }) => {
    const ids = Object.keys(dailyPBs).map(i => Number(i))
    if (!ids.length) return -1
    return Math.max(...ids)
  },
  filterByDate: ({ dailyPBs }, getters) => (date, items) => {
    const target = items || getters['all']
    return target.find(a => a.date === date)
  },
  filterByDates: ({ dailyPBs }, getters) => (dates, items) => {
    const target = items || getters['all']
    return target.filter(a => dates.includes(a.date))
  },
  filterByWeekday: ({ dailyPBs }, getters) => (wday, items) => {
    const target = items || getters['all']
    return target.filter(a => weekday[dayjs(a.date).day()] === wday)
  },
  filterBySpan: ({ dailyPBs }, getters) => (span, items) => {
    const target = items || getters['all']
    return target.filter(a => {
      const from = dayjs(span.from)
      const to = dayjs(span.to)
      const target = dayjs(a.date)
      return (
        from.isSame(target) || (target.isAfter(from) && target.isBefore(to))
      )
    })
  },
  filterByQuery: ({ dailyPBs }, getters) => (query, items) => {
    let target = items || getters['all']
    const { date, dates, span, weekday, desc } = query
    if (span) target = getters['filterBySpan'](span, target)
    if (dates) target = getters['filterByDates'](dates, target)
    if (date) {
      const finded = getters['filterByDate'](date, target)
      target = finded ? [finded] : []
    }
    if (weekday) target = getters['filterByWeekday'](weekday, target)
    return getters['sortByDate'](!!desc, target)
  },
  sortByDate: ({ dailyPBs }, getters) => (desc, items) => {
    const target = items || getters['all']
    const sorted = [...target]
    sortByDate(sorted)
    if (desc) sorted.reverse()
    return sorted
  }
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.dailyPBs = newState.dailyPBs
  },
  addDailyPB: (state, dailyPB) => {
    Vue.set(state.dailyPBs, dailyPB.id, dailyPB)
  },
  updateDailyPB: (state, { id, payload }) => {
    const dailyPB = Object.assign({}, state.dailyPBs[id], payload)
    Vue.set(state.dailyPBs, id, dailyPB)
  },
  deleteDailyPB: (state, { id }) => {
    Vue.delete(state.dailyPBs, id)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  addDailyPB: ({ commit, getters, rootState }, payload) => {
    const id = getters['latestId'] + 1
    const exist = getters['filterByDate'](payload.date)
    if (exist) {
      commit('updateDailyPB', { id: exist.id, payload })
      return
    }
    commit('addDailyPB', Object.assign({}, payload, { id }))
    ipcHandler.saveState(rootState)
  },
  updateDailyPB: ({ commit, rootState }, payload) => {
    commit('updateDailyPB', payload)
    ipcHandler.saveState(rootState)
  },
  deleteDailyPB: ({ commit, rootState }, payload) => {
    commit('deleteDailyPB', payload)
    ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
