import Attendance from '@/types/attendance'
import { DateString, DateSpan, Weekday } from '@/types'

export type Attendances = {
  [id: number]: Attendance
}

type AttendancePayload = {
  id: number
  payload: Partial<Attendance>
}

export type FilterQuery = {
  attend?: boolean
  date?: DateString
  dates?: DateString[]
  weekday?: Weekday
  span?: DateSpan
  userId?: number
  typeMonth?: boolean
  desc?: boolean
}

/* -------------------------------------------------------------------------- */
export interface S {
  attendances: Attendances
}

/* -------------------------------------------------------------------------- */

export interface G {
  get: (id: number) => Attendance | null
  all: Attendance[]
  latestId: number
  filterByAttend: (attend: boolean, items?: Attendance[]) => Attendance[]
  filterByDate: (date: DateString, items?: Attendance[]) => Attendance[]
  filterByDates: (dates: DateString[], items?: Attendance[]) => Attendance[]
  filterByWeekday: (wday: Weekday, items?: Attendance[]) => Attendance[]
  filterBySpan: (span: DateSpan, items?: Attendance[]) => Attendance[]
  filterByUser: (userId: number, items?: Attendance[]) => Attendance[]
  filterByTypeMonth: (typeMonth: boolean, items?: Attendance[]) => Attendance[]
  filterByQuery: (query: FilterQuery, items?: Attendance[]) => Attendance[]
  sortByDate: (desc = true, items?: Attendance[]) => Attendance[]
}

export interface RG {
  'attendance/get': G['get']
  'attendance/all': G['all']
  'attendance/latestId': G['latestId']
  'attendance/filterByAttend': G['filterByAttend']
  'attendance/filterByDate': G['filterByDate']
  'attendance/filterByDates': G['filterByDates']
  'attendance/filterByWeekday': G['filterByWeekday']
  'attendance/filterBySpan': G['filterBySpan']
  'attendance/filterByUser': G['filterByUser']
  'attendance/filterByQuery': G['filterByQuery']
  'attendance/sortByDate': G['sortByDate']
}

/* -------------------------------------------------------------------------- */

export interface M {
  replace: S
  addAttendance: Attendance
  updateAttendance: AttendancePayload
  deleteAttendance: { id: number }
}

export interface RM {
  'attendance/replace': G['replace']
  'attendance/addAttendance': G['addAttendance']
  'attendance/updateAttendance': G['updateAttendance']
  'attendance/deleteAttendance': G['deleteAttendance']
}

/* -------------------------------------------------------------------------- */

export interface A {
  replace: S
  addAttendance: Omit<Attendance, 'id'>
  updateAttendance: AttendancePayload
  deleteAttendance: { id: number }
  deleteAttendancesByDate: DateString
  deleteAttendancesByUser: { userId: number }
}

export interface RA {
  'attendance/replace': A['replace']
  'attendance/addAttendance': A['addAttendance']
  'attendance/updateAttendance': A['updateAttendance']
  'attendance/deleteAttendance': A['deleteAttendance']
  'attendance/deleteAttendancesByDate': A['deleteAttendancesByDate']
  'attendance/deleteAttendancesByUser': A['deleteAttendancesByUser']
}

/* -------------------------------------------------------------------------- */
