import { S, G, M, A } from './types'
import { Getters, Mutations, Actions } from 'vuex'
import Vue from 'vue'
import dayjs from 'dayjs'
import { weekday } from '@/types'
import { attendances } from '@/dummy/attendance' //TODO: delete
import { sortByDate } from '@/util'
import ipcHandler from '@/util/ipcHandler'

/* -------------------------------------------------------------------------- */

const state = (): S => ({
  attendances: {}
  // attendances: attendances // TODO: delete
})

/* -------------------------------------------------------------------------- */
const getters: Getters<S, G> = {
  get: ({ attendances }) => id => attendances[id],
  all: ({ attendances }) => Object.entries(attendances).map(a => a[1]),
  latestId: ({ attendances }) => {
    const ids = Object.keys(attendances).map(i => Number(i))
    if (!ids.length) return -1
    return Math.max(...ids)
  },
  filterByAttend: ({ attendances }, getters) => (attend, items) => {
    const target = items || getters['all']
    return target.filter(a => a.attend == attend)
  },
  filterByDate: ({ attendances }, getters) => (date, items) => {
    const target = items || getters['all']
    return target.filter(a => a.date === date)
  },
  filterByDates: ({ attendances }, getters) => (dates, items) => {
    const target = items || getters['all']
    return target.filter(a => dates.includes(a.date))
  },
  filterByWeekday: ({ attendances }, getters) => (wday, items) => {
    const target = items || getters['all']
    return target.filter(a => weekday[dayjs(a.date).day()] === wday)
  },
  filterBySpan: ({ attendances }, getters) => (span, items) => {
    const target = items || getters['all']
    return target.filter(a => {
      const from = dayjs(span.from)
      const to = dayjs(span.to)
      const target = dayjs(a.date)
      return (
        from.isSame(target) || (target.isAfter(from) && target.isBefore(to))
      )
    })
  },
  filterByUser: ({ attendances }, getters) => (userId, items) => {
    const target = items || getters['all']
    return target.filter(a => a.user === userId)
  },
  filterByTypeMonth: ({ attendances }, getters) => (typeMonth, items) => {
    const target = items || getters['all']
    return target.filter(a => String(!!a.typeMonth) == String(typeMonth))
  },
  filterByQuery: ({ attendances }, getters) => (query, items) => {
    let target = items || getters['all']
    const {
      attend,
      userId,
      date,
      dates,
      span,
      weekday,
      typeMonth,
      desc
    } = query
    if (typeof attend == 'boolean')
      target = getters['filterByAttend'](attend, target)
    if (typeof userId == 'number')
      target = getters['filterByUser'](userId, target)
    if (span) target = getters['filterBySpan'](span, target)
    if (dates) target = getters['filterByDates'](dates, target)
    if (date) target = getters['filterByDate'](date, target)
    if (typeof typeMonth == 'boolean')
      target = getters['filterByTypeMonth'](typeMonth, target)
    if (weekday) target = getters['filterByWeekday'](weekday, target)
    return getters['sortByDate'](!!desc, target)
  },
  sortByDate: ({ attendances }, getters) => (desc, items) => {
    const target = items || getters['all']
    const sorted = [...target]
    sortByDate(sorted)
    if (desc) sorted.reverse()
    return sorted
  }
}
/* -------------------------------------------------------------------------- */
const mutations: Mutations<S, M> = {
  replace: (state, newState) => {
    state.attendances = newState.attendances
  },
  addAttendance: (state, attendance) => {
    Vue.set(state.attendances, attendance.id, attendance)
  },
  updateAttendance: (state, { id, payload }) => {
    const attendance = Object.assign({}, state.attendances[id], payload)
    Vue.set(state.attendances, id, attendance)
  },
  deleteAttendance: (state, { id }) => {
    Vue.delete(state.attendances, id)
  }
}
/* -------------------------------------------------------------------------- */
const actions: Actions<S, A, G, M> = {
  replace: ({ commit, rootState }, newState) => {
    commit('replace', newState)
    ipcHandler.saveState(rootState)
  },
  addAttendance: ({ state, commit, getters, rootState }, payload) => {
    const id = getters['latestId'] + 1
    commit('addAttendance', Object.assign({}, payload, { id }))
    ipcHandler.saveState(rootState)
    return id
  },
  updateAttendance: ({ commit, rootState }, payload) => {
    commit('updateAttendance', payload)
    ipcHandler.saveState(rootState)
  },
  deleteAttendance: ({ commit, rootState }, payload) => {
    commit('deleteAttendance', payload)
    ipcHandler.saveState(rootState)
  },
  deleteAttendancesByDate: ({ commit, getters, rootState }, date) => {
    const dateData = getters['filterByDate'](date)
    const ids = dateData.map(a => a.id)
    ids.forEach(id => {
      commit('deleteAttendance', { id })
    })
    ipcHandler.saveState(rootState)
  },
  deleteAttendancesByUser: ({ commit, getters, rootState }, payload) => {
    const dateData = getters['filterByUser'](payload.userId)
    const ids = dateData.map(a => a.id)
    ids.forEach(id => {
      commit('deleteAttendance', { id })
    })
    ipcHandler.saveState(rootState)
  }
}

/* -------------------------------------------------------------------------- */

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
