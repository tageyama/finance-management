import Vue from 'vue'
import { S, G, M, A } from './types'
import Vuex, { RootState, Getters, Mutations, Actions } from 'vuex'
import user from '@/store/user'
import attendance from '@/store/attendance'
import incomeSpending from '@/store/incomeSpending'
import setting from '@/store/setting'
import dailyPB from '@/store/dailyPB'
import userPayment from '@/store/userPayment'

Vue.use(Vuex)

/* -------------------------------------------------------------------------- */

export const rootState = (): S => ({})

/* -------------------------------------------------------------------------- */

const getters: Getters<S, G> = {}

/* -------------------------------------------------------------------------- */

const mutations: Mutations<S, M> = {}

/* -------------------------------------------------------------------------- */

const actions: Actions<S, A, G, M> = {}

/* -------------------------------------------------------------------------- */

export default new Vuex.Store({
  state: rootState() as RootState,
  mutations,
  actions,
  modules: {
    user,
    attendance,
    incomeSpending,
    setting,
    dailyPB,
    userPayment
  }
})
