type Ipc = {
  on: (type: string, callback: (event: any, args: nay) => void) => void
  send: (type: string, data: any) => void
  invoke: (type: string, data: any) => Promise<any>
}

interface Window {
  ipc: Ipc
}
