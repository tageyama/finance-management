// eslint-disable-next-line
const { contextBridge, ipcRenderer } = require('electron')

//https://qiita.com/pochman/items/64b34e9827866664d436

contextBridge.exposeInMainWorld('ipc', {
  send: (type, data) => {
    // console.log(`renderInject/${type}:${data}`, 'color:blue;')
    ipcRenderer.send(type, data)
  },
  on: (type, callback) => {
    // console.log(`renderInject/${type}:${callback}`, 'color:blue;')
    ipcRenderer.on(type, callback)
  },
  invoke: (type, data) => {
    // console.log(`renderInject/${type}:${data}`, 'color:blue;')
    return ipcRenderer.invoke(type, data)
  }
})
console.log('renderInject/------load inject-----', 'color:blue;')
