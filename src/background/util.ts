import fs from 'fs'
import path from 'path'
import { MEDIA_DIR } from './settings'

export const makeDir = async (path: string): Promise<void> => {
  if (await !fs.existsSync(path)) {
    await fs.mkdirSync(path)
  }
}

export const makeMediaDir = async (): Promise<string> => {
  await makeDir(MEDIA_DIR)
  return MEDIA_DIR
}

export const getFilename = (fileName: string) => {
  return path.basename(fileName)
}

export const recursiveUnlink = async (dir: string) => {
  const files = await fs.readdirSync(dir)
  for (let i = 0; i < files.length; i++) {
    const p = path.join(dir, files[i])
    try {
      await fs.unlinkSync(p)
    } catch (e) {
      await recursiveUnlink(p)
    }
    try {
      await fs.rmdirSync(p)
    } catch (e) {
      console.log('')
    }
  }
}
