import { app, remote } from 'electron'
import path from 'path'

const _app = remote ? remote.app : app
export const isDev = () => {
  return _app.getAppPath().indexOf('app.asar') === -1
}

export const RENDER_INJECT_JS = path.join(__dirname, 'renderInject.js')

export const MEDIA_DIR = path.join(_app.getPath('userData'), 'media')
