import { RootState } from 'vuex'
import path from 'path'
import fs from 'fs'
import { makeMediaDir } from './util'

const storage = {
  get: async (key: string) => {
    console.log('storage.get', await storage._getPath(key))
    return await fs.readFileSync(await storage._getPath(key), 'utf-8')
  },
  set: async (key: string, data: string) => {
    console.log('storage.set', await storage._getPath(key))
    return await fs.writeFileSync(await storage._getPath(key), data)
  },
  _getPath: async (key: string) => {
    const dir = await makeMediaDir()
    return path.join(dir, `${key}.json`)
  }
}

export default {
  state: null as RootState | null,
  set(state: RootState | null) {
    this.state = state
  },
  getJSON() {
    return JSON.stringify(this.state)
  },
  async load() {
    try {
      return JSON.parse(
        String((await storage.get('state')) || 'null')
      ) as RootState | null
    } catch (e) {
      console.log('storage.get', e)
    }
  },
  async save(state?: RootState) {
    const _state = state || this.state
    if (!_state) return
    try {
      await storage.set('state', JSON.stringify(_state))
    } catch (e) {
      console.log('storage.save', e)
    }
  }
}
