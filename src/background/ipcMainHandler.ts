import { ipcMain } from 'electron'
import { ipcEvents } from '@/types/ipcEvents'
import storage from './storage'
import { RootState } from 'vuex'
import { dialog } from 'electron'
import { SaveDialogOptions, OpenDialogOptions } from '@/types/electronTypes'
import fs from 'fs'

const fileDialog = {
  save: (opt?: SaveDialogOptions | null) => {
    const _opt = {
      title: '保存',
      filters: [{ name: 'JSONファイル', extensions: ['json'] }]
    }
    const options = opt ? Object.assign({}, _opt, opt) : _opt
    return dialog.showSaveDialog(options)
  },
  open: (opt?: OpenDialogOptions | null) => {
    const _opt = {
      properties: ['openFile'],
      title: 'ファイル',
      filters: [
        { name: 'テキストファイル', extensions: ['txt'] },
        { name: 'JSONファイル', extensions: ['json'] }
      ]
    } as OpenDialogOptions
    const options = opt ? Object.assign({}, _opt, opt) : _opt
    return dialog.showOpenDialog(options)
  }
}

export const attachEvents = () => {
  ipcMain.on(ipcEvents.log, async (event, arg) => {
    console.log(`ipcMain/log/${arg}`)
    event.sender.send(ipcEvents.log, 'message from main')
  })
  ipcMain.on(ipcEvents.loadState, async (event, arg) => {
    try {
      console.log(`ipcMain/loadState/${arg}`)
      const store = await storage.load()
      if (!store) {
        event.sender.send(ipcEvents.error, 'storage has no data')
      }
      event.sender.send(ipcEvents.setState, store)
    } catch (e) {
      event.sender.send(ipcEvents.error, e)
    }
  })
  ipcMain.on(ipcEvents.storeState, async (event, arg) => {
    console.log(`ipcMain/storeState/${arg}`)
    try {
      await storage.save()
      event.sender.send(ipcEvents.storedState, true)  
    } catch (e) {
      event.sender.send(ipcEvents.storedState, false)  
    }
  })
  ipcMain.on(ipcEvents.saveState, async (event, arg) => {
    console.log(`ipcMain/saveState/${arg}`)
    try {
      const store = arg as RootState
      await storage.set(store)
      console.log(`ipcMain/saveState/saved`)
    } catch (e) {
      event.sender.send(ipcEvents.error, e)
    }
  })
  ipcMain.on(ipcEvents.saveFile, async (event, arg) => {
    const { options, data } = arg
    const opt = options ? (options as SaveDialogOptions) : null
    fileDialog
      .save(opt)
      .then(savedFiles => {
        if (savedFiles.canceled) return
        console.log('savedFiles', savedFiles)
        if (savedFiles.filePath) fs.writeFileSync(savedFiles.filePath,'\uFEFF'+data)
      })
      .catch(e => {
        console.log('savedFiles error', e)
        event.sender.send(ipcEvents.error, e)
      })
  })
  ipcMain.on(ipcEvents.saveStateAsFile, async (event, arg) => {
    console.log('saveStateAsFile ipc', arg)
    const opt = arg ? (arg as SaveDialogOptions) : null
    fileDialog
      .save(opt)
      .then(savedFiles => {
        if (savedFiles.canceled) return
        console.log('savedFiles', savedFiles)
        const data = storage.getJSON()
        if (savedFiles.filePath) fs.writeFileSync(savedFiles.filePath, data)
      })
      .catch(e => {
        console.log('savedFiles error', e)
        event.sender.send(ipcEvents.error, e)
      })
  })
  ipcMain.on(ipcEvents.openFile, async (event, arg) => {
    const opt = arg ? (arg as OpenDialogOptions) : null
    fileDialog
      .open(opt)
      .then(fileNames => {
        console.log('openFile', fileNames)
      })
      .catch(e => {
        console.log('openFile error', e)
        event.sender.send(ipcEvents.error, e)
      })
  })
}
