import { dateFormat, DateString } from '@/types'
import User from '@/types/user'
import { getRandomInt } from '@/util'
import { Attendances } from '@/store/attendance/types'
import { users } from './user'
import dayjs from 'dayjs'

const makeAttendance = (baseDay?: string) => {
  const user = users[getRandomInt(Object.keys(users).length)]
  const base = baseDay ? dayjs(baseDay) : dayjs()
  return {
    date: base
      .add(getRandomInt(300, -300), 'day')
      .format(dateFormat) as DateString,
    attend: !!getRandomInt(2),
    charge: user.basicCharge || null,
    user: user.id
  }
}

export const attendances = (() => {
  return [...Array(200).keys()].reduce((acc, cur) => {
    acc[cur] = { id: cur, ...makeAttendance() }
    return acc
  }, {} as Attendances)
})()
