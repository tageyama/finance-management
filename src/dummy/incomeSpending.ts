import { dateFormat, DateString } from '@/types'
import { getRandomInt } from '@/util'
import { IncomeSpendings } from '@/store/incomeSpending/types'
import { users } from './user'
import dayjs from 'dayjs'

const makeIS = (baseDay?: string) => {
  const user = users[getRandomInt(Object.keys(users).length)]
  const base = baseDay ? dayjs(baseDay) : dayjs()
  return {
    date: base
      .add(getRandomInt(300, -300), 'day')
      .format(dateFormat) as DateString,
    amount: getRandomInt(3000, -3000)
  }
}

export const incomeSpendings = (() => {
  return [...Array(200).keys()].reduce((acc, cur) => {
    acc[cur] = { id: cur, ...makeIS() }
    return acc
  }, {} as IncomeSpendings)
})()
