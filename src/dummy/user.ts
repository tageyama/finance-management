import { weekday, paymentType, prices } from '@/types'
import { getRandomInt } from '@/util'
import { Users } from '@/store/user/types'
import { colors } from '@/types/color'

const lastnames = ['山田', '高橋', '西村', '田中', '飯田', '中西']
const firstnames = ['武', '恵子', '龍', '理恵', '翔', '健司']

const makeUser = () => {
  return {
    name: `${lastnames[getRandomInt(6)]}${firstnames[getRandomInt(6)]}`,
    weekday: [weekday[getRandomInt(7)]],
    bgColor: colors[getRandomInt(100)],
    paymentType: paymentType[0],
    basicCharge: prices[getRandomInt(10)]
  }
}

export const users = (() => {
  return [...Array(40).keys()].reduce((acc, cur) => {
    acc[cur] = { id: cur, order: cur, ...makeUser() }
    return acc
  }, {} as Users)
})()
