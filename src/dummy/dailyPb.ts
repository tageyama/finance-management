import { dateFormat, DateString } from '@/types'
import { getRandomInt } from '@/util'
import { DailyPBs } from '@/store/dailyPb/types'
import { attendances } from './attendance'
import dayjs from 'dayjs'

const makePb = (date: DateString) => {
  return {
    date
  }
}

export const dailyPbs = (() => {
  const aList = Object.entries(attendances).map(a => a[1])
  const dayList = aList.map(a => a.date)
  const daySet = Array.from(new Set(dayList))
  return daySet.reduce((acc, day, i) => {
    acc[i] = { id: i, ...makePb(day) }
    return acc
  }, {} as DailyPBs)
})()
