import 'vuex'
import * as Root from '@/store/types'
import * as User from '@/store/user/types'
import * as Attendance from '@/store/attendance/types'
import * as DailyPB from '@/store/dailyPB/types'
import * as IncomeSpending from '@/store/incomeSpending/types'
import * as Setting from '@/store/setting/types'
import * as UserPayment from '@/store/userPayment/types'

declare module 'vuex' {
  type RootState = Root.S & {
    user: User.S
    attendance: Attendance.S
    dailyPB: DailyPB.S
    setting: Setting.S
    incomeSpending: IncomeSpending.S
    userPayment: UserPayment.S
  }
  type RootGetters = Root.G &
    User.RG &
    Attendance.RG &
    DailyPB.RG &
    IncomeSpending.RG &
    Setting.RG &
    UserPayment.RG
  type RootMutations = Root.M &
    User.RM &
    Attendance.RM &
    DailyPB.RM &
    IncomeSpending.RM &
    Setting.RM &
    UserPayment.RM
  type RootActions = Root.A &
    User.RA &
    Attendance.RA &
    DailyPB.RA &
    IncomeSpending.RA &
    Setting.RA &
    UserPayment.RA

  // if you add other modules
  // RootState = Root.S & {
  //   [module name]: module.S
  // }
  // RootG,M,S = Root.RG,M,A & module.RG,RM,RA
  /* -------------------------------------------------------------------------- */

  type Getters<S, G> = {
    [K in keyof G]: (
      state: S,
      getters: G,
      rootState: RootState,
      rootGetters: RootGetters
    ) => G[K]
  }

  /* -------------------------------------------------------------------------- */

  type Mutations<S, M> = {
    [K in keyof M]: (state: S, payload: M[K]) => void
  }

  /* -------------------------------------------------------------------------- */

  type ExCommit<M> = <T extends keyof M>(type: T, payload?: M[T]) => void
  type ExDispach<A> = <T extends keyof A>(type: T, payload?: A[T]) => any
  type Context<S, A, G, M> = {
    commit: ExCommit<M>
    dispatch: ExDispach<A>
    state: S
    getters: G
    rootState: RootState
    rootGetters: RootGetters
  }
  type Actions<S, A, G = {}, M = {}> = {
    [K in keyof A]: (ctx: Context<S, A, G, M>, payload: A[K]) => any
  }

  /* -------------------------------------------------------------------------- */

  interface ExStore extends Store<RootState> {
    getters: RootGetters
    commit: ExCommit<RootMutations>
    dispatch: ExDispach<RootActions>
  }

  /* -------------------------------------------------------------------------- */
}
