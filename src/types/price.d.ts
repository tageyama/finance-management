import { Weekday, PaymentType } from './index'

type User = {
  id: number
  name: string
  weekday: Weekday
  fontColor?: string
  bgColor?: string
  paymentType: PaymentType
  basicCharge?: number
}

export default User
