// 日を記録する
import { DateString } from './index'

type DailyPaymentsBalance = {
  id: number
  date: DateString //unique
}

export default DailyPaymentsBalance
