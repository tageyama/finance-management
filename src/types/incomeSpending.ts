// 収入・支出を記録する
import { DateString } from './index'

type IncomeSpending = {
  id: number
  date: DateString
  amount?: number
  memo?: string
  hideInAttendance?: boolean
}

export default IncomeSpending
