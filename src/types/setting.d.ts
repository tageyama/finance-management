import { Weekday } from '@/types'
export type WeekSetting = {
  day: Weekday
  active?: boolean
  color?: string
}
