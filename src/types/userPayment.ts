// 月ごとに利用者の支払いがあったかどうかを記録する
import { DateMonthString } from './index'

type UserPayment = {
  id: number
  date: DateMonthString
  user: number
  charged: boolean
}

export default UserPayment
