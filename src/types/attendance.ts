// 利用者の出席を記録する
import { DateString } from './index'

type Attendance = {
  id: number
  date: DateString
  user: number
  attend: boolean | null
  charge: number | null
  typeMonth?: boolean
}

export default Attendance
