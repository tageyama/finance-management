// 利用者の情報
import { Weekday, PaymentType } from './index'

type User = {
  id: number
  name: string
  weekday: Weekday[]
  fontColor?: string
  bgColor?: string
  paymentType: PaymentType
  basicCharge?: number
  memo?: string
  inactive?: boolean
  order: number
}

export const isUser = (obj: any): obj is User => {
  const keys = ['id', 'name', 'weekday', 'order']
  if (typeof obj != 'object') return false
  if (keys.some(k => !(k in obj))) return false
  return true
}

export default User
