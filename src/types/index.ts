/* -------------------------------------------------------------------------- */
export const paymentType = ['day', 'month'] as const
export type PaymentType = typeof paymentType[number]
export const paymentTypeReading = ['日ごと', '月ごと']
/* -------------------------------------------------------------------------- */
export const weekday = [
  'sun',
  'mon',
  'tue',
  'wed',
  'thu',
  'fri',
  'sat'
] as const
export type Weekday = typeof weekday[number]
export const weekdayReading = ['日', '月', '火', '水', '木', '金', '土']
/* -------------------------------------------------------------------------- */
declare const dateNominality: unique symbol
export type DateString = string & { [dateNominality]: never }
export const isDateString = (value: string): value is DateString => {
  return /^\d{4}-\d{2}-\d{2}$/.test(value)
}
export const dateFormat = 'YYYY-MM-DD'
declare const monthNominality: unique symbol
export type DateMonthString = string & { [monthNominality]: never }
export const isDateMonthString = (value: string): value is DateString => {
  return /^\d{4}-\d{2}$/.test(value)
}
export const datemonthFormat = 'YYYY-MM'
/* -------------------------------------------------------------------------- */
export type DateSpan = {
  from: DateString
  to: DateString
}
/* -------------------------------------------------------------------------- */
export const prices = [...Array(31).keys()].map(i => i * 500)
/* -------------------------------------------------------------------------- */
import dayjs from 'dayjs'
export const years = (() => {
  const current = dayjs().subtract(10, 'year')
  return [...Array(20).keys()].map(i => current.add(i, 'year').year())
})()
