export const ipcEvents = {
  error: 'error',
  log: 'log',
  // from Renderer
  storeState: 'storeState',
  saveState: 'saveState',
  loadState: 'loadState',
  saveFile: 'saveFile',
  saveStateAsFile: 'saveStateAsFile',
  openFile: 'openFile',
  // from Main
  storedState: 'storedState',
  setState: 'setState',
  getState: 'getState',
  setFile: 'setFile'
}

export type IpcEvent = keyof typeof ipcEvents
