import store from '@/store'
import {
  datemonthFormat,
  DateString,
  DateMonthString,
  DateSpan,
  Weekday,
  weekdayReading
} from '@/types'
import {
  getDailyAllUserPayment,
  getDailyOtherIncome,
  getDailyOtherSpending,
  getMonthlyUserPayment,
  getMontlyAllUserPayment,
  getMontlyOtherSpending,
  getMontlyOtherIncome
} from '@/util/paymentCalc'
import dayjs from 'dayjs'
import Attendance from '@/types/attendance'
import User, { isUser } from '@/types/user'
import UserPayment from '@/types/userPayment'
import DailyPB from '@/types/dailyPaymentsBalance'
import { balances as ABalances } from '@/views/Attendance.vue'
import { balances as PBBalances } from '@/views/PaymentsBalance.vue'

const getCsv = (array: string[][]) => {
  return array
    .map(row => row.map(col => `"${col != undefined ? col : ''}"`).join(','))
    .join('\n')
}

const formatDate = (date: string) => {
  const d = dayjs(date)
  return `${d.format('YYYY/M/DD')} ${weekdayReading[d.day()]}`
}

const getAttendanceData = (
  userId: number,
  date: DateString,
  attendances: Attendance[]
) => {
  const aList = attendances.filter(a => a.user === userId && a.date === date)
  if (!aList.length) return ''
  const attendance = aList[0] as Attendance
  if (attendance.attend) return attendance.charge
  if (!attendance.attend) return '' // 欠席状態
  return ''
}

export const getAttendanceCalcData = (key: string, date: DateString) => {
  const getDailyOtherBalance = (date: DateString) => {
    //otherBalance
    return getDailyOtherIncome(date) + getDailyOtherSpending(date)
  }
  const getBalance = (date: DateString) => {
    //balance
    const uPayments = getDailyAllUserPayment(date)
    const oIncome = getDailyOtherIncome(date)
    const oSpending = getDailyOtherSpending(date)
    return uPayments + oIncome + oSpending
  }
  switch (key) {
    case 'otherBalance':
      return getDailyOtherBalance(date)
    case 'balance':
      return getBalance(date)
    case 'income':
      return getDailyAllUserPayment(date)
  }
}

const getPaymentBalanceData = (userId: number, year: number, month: number) => {
  const amount = getMonthlyUserPayment(userId, year, month)
  if (!amount) return ''
  const date = dayjs(`${year}/${month}/01`).format(
    datemonthFormat
  ) as DateMonthString
  const payment: UserPayment | null = store.getters['userPayment/getByMonth'](
    userId,
    date
  )
  const paied = payment ? payment.charged : false
  return amount
}

const getPaymentBalanceCalcData = (
  key: string,
  year: number,
  month: number,
  weekday?: Weekday
) => {
  const getMontlyOtherBalance = (
    year: number,
    month: number,
    weekday?: Weekday
  ) => {
    const other = getMontlyOtherIncome(year, month, weekday)
    const user = getMontlyAllUserPayment(year, month, weekday)
    const spend = getMontlyOtherSpending(year, month, weekday)
    return other + user + spend
  }
  switch (key) {
    case 'otherBalance':
      return getMontlyOtherIncome(year, month, weekday)
    case 'spending':
      return getMontlyOtherSpending(year, month, weekday)
    case 'income':
      return (
        getMontlyOtherIncome(year, month, weekday) +
        getMontlyAllUserPayment(year, month, weekday)
      )
    case 'balance':
      return getMontlyOtherBalance(year, month, weekday)
  }
  return ''
}

function getTable<T, K>(
  rows: T[],
  cols: K[],
  getData: (row: T, col: K) => string
) {
  return [...rows].map(row => {
    return [...cols].map(col => getData(row, col))
  })
}

export const tableData = {
  getTable,
  attendance(query: {
    span?: DateSpan
    weekday?: Weekday
    attend?: boolean
    date?: DateString
    dates?: DateString[]
    userId?: number
    desc?: boolean
  }) {
    const users: User[] = store.getters['user/filterByQuery'](query)
    const attendances: Attendance[] = store.getters['attendance/filterByQuery'](
      query
    )
    const dailyPBs: DailyPB[] = store.getters['dailyPB/filterByQuery'](query)

    const cols = [null, ...users, ...ABalances]
    const rows = [null, ...dailyPBs]

    type B = { key: string; name: string }
    type Col = User | B | null
    type Row = DailyPB | null
    const data = getTable<Row, Col>(rows, cols, (row, col) => {
      if (row == null && col == null) return ''
      if (row && col == null) return formatDate(row.date)
      if (row == null && col) return col.name
      if (row && col) {
        if (isUser(col)) {
          return getAttendanceData(col.id, row.date, attendances)
        }
        return getAttendanceCalcData(col.key, row.date)
      }
      return ''
    })
    return getCsv(data)
  },
  paymentBalance(query: { year: number; weekday?: Weekday; desc?: boolean }) {
    const { year, weekday } = query
    const months = [...Array(12).keys()].map(i => i + 1)
    const users: User[] = store.getters['user/filterByQuery'](query)
    const balances: { key: string; name: string }[] = PBBalances

    const rows = [null, ...months]
    const cols = [null, ...users, ...balances]

    type B = { key: string; name: string }
    type Row = number | null
    type Col = User | B | null

    const data = getTable<Row, Col>(rows, cols, (row, col) => {
      if (row == null && col == null) return ''
      if (row != null && col == null) return `${year}/${row}`
      if (row == null && col != null) return col.name
      if (row != null && col != null) {
        if (isUser(col)) {
          return getPaymentBalanceData(col.id, year, row)
        }
        return getPaymentBalanceCalcData(col.key, year, row, weekday)
      }
      return ''
    })
    return getCsv(data)
  },
  users() {
    const users = store.getters['user/all']
    const row = ['名前', 'メモ']
    const data = [row, ...users.map((u: User) => [u.name, u.memo])]
    return getCsv(data)
  }
}
