import { ipcEvents } from '@/types/ipcEvents'
import { RootState } from 'vuex'
import store from '@/store'
import { ExStore } from 'vuex'

export const loadStore = (state: RootState) => {
  const $store = store as ExStore
  store.dispatch('attendance/replace', state.attendance)
  store.dispatch('dailyPB/replace', state.dailyPB)
  store.dispatch('incomeSpending/replace', state.incomeSpending)
  store.dispatch('setting/replace', state.setting)
  store.dispatch('user/replace', state.user)
  store.dispatch('userPayment/replace', state.userPayment)
}

// for debugging without electron
if (window.ipc == null) {
  window.ipc = {
    on: (...args) => {
      console.log(args)
    },
    send: (...args) => {
      console.log(args)
    },
    invoke: (...args) => {
      console.log(args)
      return new Promise((resolve, reject) => {
        resolve(args)
      })
    }
  }
}

window.ipc.on(ipcEvents.setState, (e, args) => {
  if (!args) return
  const state = args as RootState
  loadStore(state)
})

window.ipc.on(ipcEvents.error, (e, args) => {
  console.error(args)
})

const ipcHandler = {
  storeState() {
    return new Promise<boolean>((resolve, reject) => {
     window.ipc.on(ipcEvents.storedState, (e, args) => {
        if (args) {
         resolve(true)
       } else {
         reject()
       }
      })
      window.ipc.send(ipcEvents.storeState, null)
    })
  },
  saveState(state: RootState) {
    console.log('ipcHandler.saveState')
    window.ipc.send(ipcEvents.saveState, state)
  },
  loadState() {
    window.ipc.send(ipcEvents.loadState, null)
  },
  saveCsv(data: string) {
    const options = {
      title: '保存',
      filters: [{ name: 'エクセルファイル', extensions: ['csv'] }]
    }
    window.ipc.send(ipcEvents.saveFile, { options, data })
  },
  saveStateAsFile() {
    console.log('saveStateAsFile')
    const opt = {
      title: '保存',
      filters: [{ name: 'JSONファイル', extensions: ['json'] }]
    }
    window.ipc.send(ipcEvents.saveStateAsFile, opt)
  }
}

export default ipcHandler
