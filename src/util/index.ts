import { DateString } from '@/types'
import { RootState } from 'vuex'

export const getRandomInt = (max: number, min?: number) => {
  const _min = min || 0
  return Math.floor(Math.random() * Math.floor(max - _min)) + _min
}

type HasDate = {
  date: DateString
}
export const sortByDate = <T extends HasDate>(items: T[]) => {
  return items.sort((a, b) => {
    if (a.date < b.date) return -1
    if (a.date > b.date) return 1
    return 0
  })
}

export const os = window.navigator.userAgent.indexOf('Windows')
  ? 'win'
  : window.navigator.userAgent.indexOf('Mac') && 'mac'

export const isState = (obj: any): obj is RootState => {
  const modules = [
    'attendance',
    'dailyPB',
    'user',
    'incomeSpending',
    'setting',
    'userPayment'
  ]
  if (typeof obj != 'object') return false
  if (modules.some(m => !(m in obj))) return false
  return true
}
