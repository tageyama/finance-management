import store from '@/store'
import { dateFormat, DateString, Weekday } from '@/types'
import dayjs from 'dayjs'
import Attendance from '@/types/attendance'
import User from '@/types/user'
import IncomeSpending from '@/types/incomeSpending'

export const getMonthlyUserPayment = (
  userId: number,
  year: number,
  month: number
) => {
  const user = store.getters['user/get'](userId)
  if (!user) return 0
  const from = dayjs(`${year}/${month}/1`).format(dateFormat) as DateString
  const to = dayjs(`${year}/${month}/1`)
    .add(1, 'month')
    .format(dateFormat) as DateString
  const query = {
    userId,
    attend: true,
    typeMonth: false,
    span: {
      from,
      to
    }
  }
  const attendances = store.getters['attendance/filterByQuery'](query)
  if (attendances.length) {
    return attendances.reduce((acc: number, cur: Attendance) => {
      return acc + Number(cur.charge)
    }, 0)
  }

  query.typeMonth = true
  const mAttendances = store.getters['attendance/filterByQuery'](query)
  if (mAttendances.length) {
    return mAttendances[0].charge
  }

  return 0
}

export const getMontlyAllUserPayment = (
  year: number,
  month: number,
  weekday?: Weekday
) => {
  const query = { weekday }
  const userIds = store.getters['user/filterByQuery'](query).map(
    (u: User) => u.id
  )
  return userIds.reduce((acc: number, cur: number) => {
    return acc + getMonthlyUserPayment(cur, year, month)
  }, 0)
}

export const getMontlyOtherSpending = (
  year: number,
  month: number,
  weekday?: Weekday
) => {
  const from = dayjs(`${year}/${month}/1`).format(dateFormat) as DateString
  const to = dayjs(`${year}/${month}/1`)
    .add(1, 'month')
    .format(dateFormat) as DateString
  const query = {
    span: {
      from,
      to
    },
    weekday
  }
  const incomeSpendings = store.getters['incomeSpending/filterByQuery'](query)
  const incomes = incomeSpendings.filter(
    (is: IncomeSpending) => is.amount && Number(is.amount) < 0
  )
  return incomes.reduce((acc: number, cur: IncomeSpending) => {
    return acc + Number(cur.amount)
  }, 0)
}

export const getMontlyOtherIncome = (
  year: number,
  month: number,
  weekday?: Weekday
) => {
  const from = dayjs(`${year}/${month}/1`).format(dateFormat) as DateString
  const to = dayjs(`${year}/${month}/1`)
    .add(1, 'month')
    .format(dateFormat) as DateString
  const query = {
    span: {
      from,
      to
    },
    weekday
  }
  const incomeSpendings = store.getters['incomeSpending/filterByQuery'](query)
  const incomes = incomeSpendings.filter(
    (is: IncomeSpending) => is.amount && Number(is.amount) > 0
  )
  return incomes.reduce((acc: number, cur: IncomeSpending) => {
    return acc + Number(cur.amount)
  }, 0)
}

export const getDailyAllUserPayment = (date: DateString) => {
  const pb = store.getters['dailyPB/filterByDate'](date)
  const q = {
    date,
    attend: true,
    typeMonth: false,
    desc: false
  }
  const attendances = store.getters['attendance/filterByQuery'](q)
  return attendances.reduce(
    (acc: number, a: Attendance) => acc + (Number(a.charge) || 0),
    0
  )
}

export const getDailyOtherIncome = (
  date: DateString,
  withOutMonthType = false
) => {
  const query = { date }
  const incomeSpendings = store.getters['incomeSpending/filterByQuery'](query)
  const incomes = incomeSpendings.filter(
    (is: IncomeSpending) => is.amount && is.amount > 0
  )
  const _incomes = withOutMonthType
    ? incomes.filter((is: IncomeSpending) => !is.hideInAttendance)
    : incomes
  return _incomes.reduce((acc: number, cur: IncomeSpending) => {
    return acc + (Number(cur.amount) || 0)
  }, 0)
}

export const getDailyOtherSpending = (
  date: DateString,
  withOutMonthType = false
) => {
  const query = { date }
  const incomeSpendings = store.getters['incomeSpending/filterByQuery'](query)
  const incomes = incomeSpendings.filter(
    (is: IncomeSpending) => is.amount && is.amount < 0
  )
  const _incomes = withOutMonthType
    ? incomes.filter((is: IncomeSpending) => !is.hideInAttendance)
    : incomes
  return _incomes.reduce((acc: number, cur: IncomeSpending) => {
    return acc + Number(cur.amount)
  }, 0)
}
