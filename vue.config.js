module.exports = {
  transpileDependencies: ['vuetify'],
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        mac: {
          icon: 'build/icons/icon.icns'
        },
        win: {
          icon: 'build/icons/icon.ico'
        }
      },
      chainWebpackMainProcess: config => {
        // Chain webpack config for electron main process only
        config.entry('renderInject').add('./src/background/renderInject.js')
        config.output.filename('[name].js')
      }
    }
  }
}
