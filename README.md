# attendance managemant

## about this project
This is an open source repository for an Excel-like attendance management desktop application.
By registering a list of users, you can easily manage the attendance of users from a table.
The amount of money can be set for each user, and daily and monthly earnings are automatically calculated when the attendance table is filled out.

The application can be built for Mac and Windows using Electron.

## check the demo video
[Demo Video](https://youtu.be/mMuKVZUkfWg)
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
**Warning**
this project uses Electron.
By serving it without Electron, this app works only limitedly.

```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
